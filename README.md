# Leveraging Knative

Slides (demo.slide), yaml files and scripts used for Knative sessions.

## Prerequisite

### Go and Present
Install go and present tool.
```bash
dnf install go #Ignore if you already have go
go get golang.org/x/tools/present
```


### Minikube cluster
Assuming that minikube is installed, start it using:

```bash
minikube start --memory=8192 --cpus=4 \
  --kubernetes-version=v1.11.5 \
  --vm-driver=kvm2 \
  --disk-size=30g \
  --extra-config=apiserver.enable-admission-plugins="LimitRanger,NamespaceExists,NamespaceLifecycle,ResourceQuota,ServiceAccount,DefaultStorageClass,MutatingAdmissionWebhook"
  ```
  
  ### Istio
  ```bash
  kubectl apply --filename https://github.com/knative/serving/releases/download/v0.4.0/istio-crds.yaml &&
curl -L https://github.com/knative/serving/releases/download/v0.4.0/istio.yaml \
  | sed 's/LoadBalancer/NodePort/' \
  | kubectl apply --filename -

# Label the default namespace with istio-injection=enabled.
kubectl label namespace default istio-injection=enabled
```
Wait for above command completion:

```bash
kubectl get pods --namespace istio-system -w

```
Use CTRL + C to exit watch mode

### Knative Serving
Run:  
```bash
curl -L https://github.com/knative/serving/releases/download/v0.4.0/serving.yaml \
  | sed 's/LoadBalancer/NodePort/' \
  | kubectl apply --filename -
```

Wait for its completion

```bash
kubectl get pods --namespace knative-serving -w
```
Use CTRL + C to exit watch mode

### Knative Build
Run:
```bash
kubectl apply --filename https://github.com/knative/build/releases/download/v0.4.0/build.yaml
```

Wait for its completion
```bash
kubectl get pods --namespace knative-build -w
```
Use CTRL + C to exit watch mode


### Knative Eventing

Run:
```bash
kubectl apply --filename https://github.com/knative/eventing-sources/releases/download/v0.4.0/release.yaml
kubectl apply --filename https://github.com/knative/eventing/releases/download/v0.4.0/release.yaml
```
Wait for its completion

```bash
kubectl get pods --namespace knative-eventing -w
kubectl get pods --namespace knative-sources -w
```


### Tekton Pipelines

```bash
kubectl apply --filename https://storage.googleapis.com/knative-releases/build-pipeline/latest/release.yaml
```

```bash
kubectl get pods --namespace tekton-pipelines -w
```


## Secrets and Service Account
Image building and pushingt to registry tasks are presented for Knative-Build and Tekton pipelines.
For pushing image to registry, file secret.yaml need to be edited with credentials for registry. Instructions are available in the file.
Then you need to apply secret and service account having secret.

```bash
kubectl apply -f secret.yaml
kubectl apply -f sakbaig.yaml
kubectl describe sa kbaig
```

## Start Presentation
From the root directory, run:
```bash
present .
```
