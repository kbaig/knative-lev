Knative
05 Mar 2019

Khurram Baig
kbaig@redhat.com

* Serverless

- Small, single-purpose functions that are invoked via events.
- Code is using compute resources only while serving requests.
- On premise vs cloud

* Why Knative

- Zero-downtime deployment.
- Scale base on demand
- Consistency.

* Knative

Utilizing Kubernetes and Ingress Controller like istio or gloo.


- Build
- Serving
- Eventing


* Serving

- Deploy a prebuilt image
- Maintain point in time snapshot
- Automatic scaling

.image Serving.png

* Serving - Configuration & Revision

- Configurations atleast contains name and reference to container image to deploy.
- Revision is the immutable snapshot of configuration and code.
- Modification of configuration creates a new revision automatically.
- Service which takes care of deployment, rollback and routing.

* Serving - Configuration & Revision

.code servingHelloWorldGo.yaml

.play applyHelloServing.sh
.play -edit reqHelloServing.sh

* Serving - Route
Route map HTTP points to one or more revision names.

.code helloWorldRoute.yaml

.play applyHelloRoute.sh

.play reqHelloServingRoute.sh

* Serving Autoscaler and Activator

.image AutoscalerActivator.png

.play activatorAutoscaler.sh

* Build

- "A Build is a custom resource in Knative that allows you to define a process that runs to completion and can provide status."

So Knative build is CRD to fetch code, build image, push it and convey the status of this process.

- Build can include multiple `steps`.
- Each steps have builder which is a container image you create for a task.
- Each step spins up an init-containers.

* Build

.code helloworldBuild.yaml


.play -edit applyHellobuild.sh

* Build Templates

.code dockerBuildTemplate.yaml

.play applyBT.sh

* Use Build Templates

.code useBuildTempalate.yaml

.play applyUseBT.sh


* Knative Eventing
Loosely coupled services
- Event Sources (k8s, github, pub/sub, container)
- Channels (Buffers)
- Subscriptions- Forwards messages to other services or channels

* Channel

.code eventing/channel.yaml
.play eventing/applyChannel.sh

* Event Source

.code eventing/k8s-events.yaml
.play eventing/applyEventSource.sh

* Subscriptions

.code -edit eventing/subscription.yaml

* Subscriptions

.play -edit eventing/applySubscription.sh


* Tekton Pipelines

- Configure and run CI/CD style pipelines
- Task
- Taskrun
- Pipelineresources
- Pipeline
- PipelineRun

* Tasks

.code taskrun/hello.yaml

* Tasks Run

.play -edit taskrun/applyHello.sh

.play -edit taskrun/describeHello.sh

* Tasks and PipelineResources

.code -edit taskrun/gitPipeRsc.yaml

* Tasks and PipelineResources

.code -edit taskrun/imagePipeRsc.yaml

* Tasks

.code -edit taskrun/task.yaml

* Task Run

.code -edit taskrun/taskrun.yaml

* Task Run contd

.play taskrun/applyTasks.sh

.play taskrun/describeTask.sh

.play -edit taskrun/Log.sh


* Pipeline

Pipeline defines a list of tasks, also if any of the input is to be used in next step. Also, it indicates order of executing tasks.

.code -edit pipeline/pipeline.yaml

* PipelineRun

.code pipeline/pipelinerun.yaml

* PipelineRun

.play -edit pipeline/applyPipelines.sh